from globack_utils.globack.entity_manager import EntityManager
from globack_utils.globack.middleware import create_context
from globack_utils.globack.middleware import habi_middleware


# EC2/Fargate Event task
@habi_middleware
def main(event, context):
    # Entity manager initialization
    manager = EntityManager(context)


if __name__ == '__main__':
    create_context(main, country='CO', conn_type='read', stage='dev')
