# {{cookiecutter.project_name}} Documentation

## Introduction


### Prerequisites


## Configuration



## Execution Flow


## References

### Authors and Collaborators
| Developer               | Slack Team                  | 
|-------------------------|-----------------------------|
| {{cookiecutter.author}} | {{cookiecutter.slack_team}} |
| -                       | -                           |
| -                       | -                           |
